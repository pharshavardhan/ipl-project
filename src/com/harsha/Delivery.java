package com.harsha;

/**
 * A class to store details of a delivery in cricket match
 *
 * @author Harsha
 */

class Delivery {
    private boolean isSuperOver;
    private int matchId;
    private int inning;
    private int over;
    private int ball;
    private int wideRuns;
    private int byeRuns;
    private int legByeRuns;
    private int noBallRuns;
    private int penaltyRuns;
    private int batsmanRuns;
    private int extraRuns;
    private int totalRuns;
    private String battingTeam;
    private String bowlingTeam;
    private String batsman;
    private String nonStriker;
    private String bowler;
    private String playerDismissed;
    private String dismissalKind;
    private String fielder;

    public int getMatchId() {
        return matchId;
    }

    public void setMatchId(int matchId) {
        this.matchId = matchId;
    }

    public void setMatchId(String matchId) {
        this.matchId = Integer.parseInt(matchId);
    }

    public int getInning() {
        return inning;
    }

    public void setInning(int inning) {
        this.inning = inning;
    }

    public void setInning(String inning) {
        this.inning = Integer.parseInt(inning);
    }

    public String getBattingTeam() {
        return battingTeam;
    }

    public void setBattingTeam(String battingTeam) {
        if (battingTeam.equals("Rising Pune Supergiants")) {
            battingTeam = "Rising Pune Supergiant";
        }
        this.battingTeam = battingTeam;
    }

    public String getBowlingTeam() {
        return bowlingTeam;
    }

    public void setBowlingTeam(String bowlingTeam) {
        if (bowlingTeam.equals("Rising Pune Supergiants")) {
            bowlingTeam = "Rising Pune Supergiant";
        }
        this.bowlingTeam = bowlingTeam;
    }

    public int getOver() {
        return over;
    }

    public void setOver(int over) {
        this.over = over;
    }

    public void setOver(String over) {
        this.over = Integer.parseInt(over);
    }

    public int getBall() {
        return ball;
    }

    public void setBall(int ball) {
        this.ball = ball;
    }

    public void setBall(String ball) {
        this.ball = Integer.parseInt(ball);
    }

    public String getBatsman() {
        return batsman;
    }

    public void setBatsman(String batsman) {
        this.batsman = batsman;
    }

    public String getNonStriker() {
        return nonStriker;
    }

    public void setNonStriker(String nonStriker) {
        this.nonStriker = nonStriker;
    }

    public String getBowler() {
        return bowler;
    }

    public void setBowler(String bowler) {
        this.bowler = bowler;
    }

    public boolean isSuperOver() {
        return isSuperOver;
    }

    public void setSuperOver(boolean superOver) {
        isSuperOver = superOver;
    }

    public void setSuperOver(String superOver) {
        isSuperOver = superOver.equals("1");
    }

    public int getWideRuns() {
        return wideRuns;
    }

    public void setWideRuns(int wideRuns) {
        this.wideRuns = wideRuns;
    }

    public void setWideRuns(String wideRuns) {
        this.wideRuns = Integer.parseInt(wideRuns);
    }

    public int getByeRuns() {
        return byeRuns;
    }

    public void setByeRuns(int byeRuns) {
        this.byeRuns = byeRuns;
    }

    public void setByeRuns(String byeRuns) {
        this.byeRuns = Integer.parseInt(byeRuns);
    }

    public int getLegByeRuns() {
        return legByeRuns;
    }

    public void setLegByeRuns(int legByeRuns) {
        this.legByeRuns = legByeRuns;
    }

    public void setLegByeRuns(String legByeRuns) {
        this.legByeRuns = Integer.parseInt(legByeRuns);
    }

    public int getNoBallRuns() {
        return noBallRuns;
    }

    public void setNoBallRuns(int noBallRuns) {
        this.noBallRuns = noBallRuns;
    }

    public void setNoBallRuns(String noBallRuns) {
        this.noBallRuns = Integer.parseInt(noBallRuns);
    }

    public int getPenaltyRuns() {
        return penaltyRuns;
    }

    public void setPenaltyRuns(int penaltyRuns) {
        this.penaltyRuns = penaltyRuns;
    }

    public void setPenaltyRuns(String penaltyRuns) {
        this.penaltyRuns = Integer.parseInt(penaltyRuns);
    }

    public int getBatsmanRuns() {
        return batsmanRuns;
    }

    public void setBatsmanRuns(int batsmanRuns) {
        this.batsmanRuns = batsmanRuns;
    }

    public void setBatsmanRuns(String batsmanRuns) {
        this.batsmanRuns = Integer.parseInt(batsmanRuns);
    }

    public int getExtraRuns() {
        return extraRuns;
    }

    public void setExtraRuns(int extraRuns) {
        this.extraRuns = extraRuns;
    }

    public void setExtraRuns(String extraRuns) {
        this.extraRuns = Integer.parseInt(extraRuns);
    }

    public int getTotalRuns() {
        return totalRuns;
    }

    public void setTotalRuns(int totalRuns) {
        this.totalRuns = totalRuns;
    }

    public void setTotalRuns(String totalRuns) {
        this.totalRuns = Integer.parseInt(totalRuns);
    }

    public String getPlayerDismissed() {
        return playerDismissed;
    }

    public void setPlayerDismissed(String playerDismissed) {
        this.playerDismissed = playerDismissed;
    }

    public String getDismissalKind() {
        return dismissalKind;
    }

    public void setDismissalKind(String dismissalKind) {
        this.dismissalKind = dismissalKind;
    }

    public String getFielder() {
        return fielder;
    }

    public void setFielder(String fielder) {
        this.fielder = fielder;
    }
}
