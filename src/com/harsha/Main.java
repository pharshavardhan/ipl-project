package com.harsha;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.sql.*;
import java.util.*;

/**
 * Main program processes IPL data(deliveries.csv, matches.csv) from 2008-2017 and prints
 * some stats to the screen.
 *
 * @author Harsha
 */
public class Main {
    private static final int MATCH_ID = 0;
    private static final int MATCH_SEASON = 1;
    private static final int MATCH_CITY = 2;
    private static final int MATCH_DATE = 3;
    private static final int MATCH_TEAM1 = 4;
    private static final int MATCH_TEAM2 = 5;
    private static final int MATCH_TOSS_WINNER = 6;
    private static final int MATCH_TOSS_DECISION = 7;
    private static final int MATCH_RESULT = 8;
    private static final int MATCH_DL_APPLIED = 9;
    private static final int MATCH_WINNER = 10;
    private static final int MATCH_WIN_BY_RUNS = 11;
    private static final int MATCH_WIN_BY_WICKETS = 12;
    private static final int MATCH_PLAYER_OF_MATCH = 13;
    private static final int MATCH_VENUE = 14;
    private static final int MATCH_UMPIRE1 = 15;
    private static final int MATCH_UMPIRE2 = 16;

    private static final int DELIVERY_MATCH_ID = 0;
    private static final int DELIVERY_INNING = 1;
    private static final int DELIVERY_BATTING_TEAM = 2;
    private static final int DELIVERY_BOWLING_TEAM = 3;
    private static final int DELIVERY_OVER = 4;
    private static final int DELIVERY_BALL = 5;
    private static final int DELIVERY_BATSMAN = 6;
    private static final int DELIVERY_NON_STRIKER = 7;
    private static final int DELIVERY_BOWLER = 8;
    private static final int DELIVERY_SUPER_OVER = 9;
    private static final int DELIVERY_WIDE_RUNS = 10;
    private static final int DELIVERY_BYE_RUNS = 11;
    private static final int DELIVERY_LEG_BYE_RUNS = 12;
    private static final int DELIVERY_NO_BALL_RUNS = 13;
    private static final int DELIVERY_PENALTY_RUNS = 14;
    private static final int DELIVERY_BATSMAN_RUNS = 15;
    private static final int DELIVERY_EXTRA_RUNS = 16;
    private static final int DELIVERY_TOTAL_RUNS = 17;
    private static final int DELIVERY_PLAYER_DISMISSED = 18;
    private static final int DELIVERY_DISMISSAL_KIND = 19;
    private static final int DELIVERY_FIELDER = 20;

    private static final String PATH_TO_MATCHES = "./resources/matches.csv";
    private static final String PATH_TO_DELIVERIES = "./resources/deliveries.csv";
    private static final String DELIMITER = ",";

    private static final String MATCHES_QUERY = "SELECT * FROM matches";
    private static final String DELIVERIES_QUERY = "SELECT * FROM deliveries";

    private static final Map<Integer, String> idSeasonMap = new HashMap<>();

    public static void main(String[] args) {

        try (Connection databaseConnection = getDatabaseConnection()) {
            List<Match> matches = getMatchesData(databaseConnection);
            List<Delivery> deliveries = getDeliveriesData(databaseConnection);

            findMatchesPlayedPerSeason(matches);
            findNumberOfWinsPerTeam(matches);
            findPlayersWithAtLeast1PlayerOfMatch(matches);
            findExtraRunsConcededByEachTeamIn2016(deliveries);
            findTopEconomicalBowlersIn2015(deliveries);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    private static Connection getDatabaseConnection() throws SQLException {
        Connection connection = null;

        try (FileInputStream fileInputStream = new FileInputStream("./resources/dbConfig.properties")) {
            Properties dbProperties = new Properties();
            dbProperties.load(fileInputStream);

            String url = dbProperties.getProperty("url");
            String user = dbProperties.getProperty("user");
            String password = dbProperties.getProperty("password");

            connection = DriverManager.getConnection(url, user, password);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return connection;
    }

    private static List<Delivery> getDeliveriesData() {
        List<Delivery> deliveries = new ArrayList<>();

        try (Scanner scanner = new Scanner(Path.of(PATH_TO_DELIVERIES))) {
            scanner.nextLine();

            while (scanner.hasNext()) {
                String row = scanner.nextLine();
                String[] fields = row.split(DELIMITER);

                Delivery delivery = new Delivery();
                delivery.setMatchId(fields[DELIVERY_MATCH_ID]);
                delivery.setInning(fields[DELIVERY_INNING]);
                delivery.setBattingTeam(fields[DELIVERY_BATTING_TEAM]);
                delivery.setBowlingTeam(fields[DELIVERY_BOWLING_TEAM]);
                delivery.setOver(fields[DELIVERY_OVER]);
                delivery.setBall(fields[DELIVERY_BALL]);
                delivery.setBatsman(fields[DELIVERY_BATSMAN]);
                delivery.setNonStriker(fields[DELIVERY_NON_STRIKER]);
                delivery.setBowler(fields[DELIVERY_BOWLER]);
                delivery.setSuperOver(fields[DELIVERY_SUPER_OVER]);
                delivery.setWideRuns(fields[DELIVERY_WIDE_RUNS]);
                delivery.setByeRuns(fields[DELIVERY_BYE_RUNS]);
                delivery.setLegByeRuns(fields[DELIVERY_LEG_BYE_RUNS]);
                delivery.setNoBallRuns(fields[DELIVERY_NO_BALL_RUNS]);
                delivery.setPenaltyRuns(fields[DELIVERY_PENALTY_RUNS]);
                delivery.setBatsmanRuns(fields[DELIVERY_BATSMAN_RUNS]);
                delivery.setExtraRuns(fields[DELIVERY_EXTRA_RUNS]);
                delivery.setTotalRuns(fields[DELIVERY_TOTAL_RUNS]);
                delivery.setPlayerDismissed(fields.length > DELIVERY_PLAYER_DISMISSED ?
                        fields[DELIVERY_PLAYER_DISMISSED] : null);
                delivery.setDismissalKind(fields.length > DELIVERY_DISMISSAL_KIND ?
                        fields[DELIVERY_DISMISSAL_KIND] : null);
                delivery.setFielder(fields.length > DELIVERY_FIELDER ? fields[DELIVERY_FIELDER] : null);

                deliveries.add(delivery);
            }
        } catch (IOException e) {
            System.out.println("file not found: " + PATH_TO_DELIVERIES);
        }
        return deliveries;
    }

    private static List<Delivery> getDeliveriesData(Connection connection) {
        List<Delivery> deliveries = new ArrayList<>();

        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(DELIVERIES_QUERY)) {

            while (resultSet.next()) {
                Delivery delivery = new Delivery();

                delivery.setMatchId(resultSet.getString(DELIVERY_MATCH_ID + 1));
                delivery.setInning(resultSet.getString(DELIVERY_INNING + 1));
                delivery.setBattingTeam(resultSet.getString(DELIVERY_BATTING_TEAM + 1));
                delivery.setBowlingTeam(resultSet.getString(DELIVERY_BOWLING_TEAM + 1));
                delivery.setOver(resultSet.getString(DELIVERY_OVER + 1));
                delivery.setBall(resultSet.getString(DELIVERY_BALL + 1));
                delivery.setBatsman(resultSet.getString(DELIVERY_BATSMAN + 1));
                delivery.setNonStriker(resultSet.getString(DELIVERY_NON_STRIKER + 1));
                delivery.setBowler(resultSet.getString(DELIVERY_BOWLER + 1));
                delivery.setSuperOver(resultSet.getString(DELIVERY_SUPER_OVER + 1));
                delivery.setWideRuns(resultSet.getString(DELIVERY_WIDE_RUNS + 1));
                delivery.setByeRuns(resultSet.getString(DELIVERY_BYE_RUNS + 1));
                delivery.setLegByeRuns(resultSet.getString(DELIVERY_LEG_BYE_RUNS + 1));
                delivery.setNoBallRuns(resultSet.getString(DELIVERY_NO_BALL_RUNS + 1));
                delivery.setPenaltyRuns(resultSet.getString(DELIVERY_PENALTY_RUNS + 1));
                delivery.setBatsmanRuns(resultSet.getString(DELIVERY_BATSMAN_RUNS + 1));
                delivery.setExtraRuns(resultSet.getString(DELIVERY_EXTRA_RUNS + 1));
                delivery.setTotalRuns(resultSet.getString(DELIVERY_TOTAL_RUNS + 1));
                delivery.setPlayerDismissed(resultSet.getString(DELIVERY_PLAYER_DISMISSED + 1));
                delivery.setDismissalKind(resultSet.getString(DELIVERY_DISMISSAL_KIND + 1));
                delivery.setFielder(resultSet.getString(DELIVERY_FIELDER + 1));

                deliveries.add(delivery);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return deliveries;
    }

    private static List<Match> getMatchesData(Connection connection) {
        List<Match> matches = new ArrayList<>();

        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(MATCHES_QUERY)) {

            while (resultSet.next()) {
                Match match = new Match();
                match.setId(resultSet.getString(MATCH_ID + 1));
                match.setSeason(resultSet.getString(MATCH_SEASON + 1));
                match.setCity(resultSet.getString(MATCH_CITY + 1));
                match.setDate(resultSet.getString(MATCH_DATE + 1));
                match.setTeam1(resultSet.getString(MATCH_TEAM1 + 1));
                match.setTeam2(resultSet.getString(MATCH_TEAM2 + 1));
                match.setTossWinner(resultSet.getString(MATCH_TOSS_WINNER + 1));
                match.setTossDecision(resultSet.getString(MATCH_TOSS_DECISION + 1));
                match.setResult(resultSet.getString(MATCH_RESULT + 1));
                match.setDlApplied(resultSet.getString(MATCH_DL_APPLIED + 1));
                match.setWinner(resultSet.getString(MATCH_WINNER + 1));
                match.setWinByRuns(resultSet.getString(MATCH_WIN_BY_RUNS + 1));
                match.setWinByWickets(resultSet.getString(MATCH_WIN_BY_WICKETS + 1));
                match.setPlayerOfMatch(resultSet.getString(MATCH_PLAYER_OF_MATCH + 1));
                match.setVenue(resultSet.getString(MATCH_VENUE + 1));
                match.setUmpire1(resultSet.getString(MATCH_UMPIRE1 + 1));
                match.setUmpire2(resultSet.getString(MATCH_UMPIRE2 + 1));

                matches.add(match);
                idSeasonMap.put(match.getId(), match.getSeason());
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return matches;
    }

    private static List<Match> getMatchesData() {
        List<Match> matches = new ArrayList<>();

        try (Scanner scanner = new Scanner(Path.of(PATH_TO_MATCHES))) {
            scanner.nextLine();

            while (scanner.hasNext()) {
                String row = scanner.nextLine();
                String[] fields = row.split(DELIMITER);

                Match match = new Match();
                match.setId(fields[MATCH_ID]);
                match.setSeason(fields[MATCH_SEASON]);
                match.setCity(fields[MATCH_CITY]);
                match.setDate(fields[MATCH_DATE]);
                match.setTeam1(fields[MATCH_TEAM1]);
                match.setTeam2(fields[MATCH_TEAM2]);
                match.setTossWinner(fields[MATCH_TOSS_WINNER]);
                match.setTossDecision(fields[MATCH_TOSS_DECISION]);
                match.setResult(fields[MATCH_RESULT]);
                match.setDlApplied(fields[MATCH_DL_APPLIED]);
                match.setWinner(fields[MATCH_WINNER]);
                match.setWinByRuns(fields[MATCH_WIN_BY_RUNS]);
                match.setWinByWickets(fields[MATCH_WIN_BY_WICKETS]);
                match.setPlayerOfMatch(fields[MATCH_PLAYER_OF_MATCH]);
                match.setVenue(fields[MATCH_VENUE]);
                match.setUmpire1(fields.length > MATCH_UMPIRE1 ? fields[MATCH_UMPIRE1] : null);
                match.setUmpire2(fields.length > MATCH_UMPIRE2 ? fields[MATCH_UMPIRE2] : null);

                matches.add(match);
                idSeasonMap.put(match.getId(), match.getSeason());
            }
        } catch (IOException e) {
            System.out.println("file not found: " + PATH_TO_MATCHES);
        }
        return matches;
    }

    /**
     * increments the integer data present at given key by 'value' if key already exists else inserts (key, value) into
     * given Map denoted by dictionary
     *
     * @param dictionary Any Map<String, Integer> reference used to store occurrences of key
     * @param key        a String object whose frequencies we are tracking
     * @param value      an int value denoting occurrences of key
     */
    private static void updateMapData(Map<String, Integer> dictionary, String key, int value) {
        if (dictionary.containsKey(key)) {
            dictionary.put(key, dictionary.get(key) + value);
        } else {
            dictionary.put(key, value);
        }
    }

    /**
     * prints the data present in given Map object in neatly formatted way
     *
     * @param dictionary Any Map<String, Integer>
     * @param lHead      A heading to describe the data present in keys
     * @param rHead      A heading to describe the data present in values
     * @param lWidth     width of space to print data in key
     * @param rWidth     width of space to print data in value
     */
    private static void printMapData(Map<String, Integer> dictionary, String lHead, String rHead, int lWidth, int rWidth) {
        System.out.println("    ========================================     \n");
        System.out.printf("%-" + lWidth + "s : %-" + rWidth + "s\n", lHead, rHead);

        for (Map.Entry<String, Integer> entry : dictionary.entrySet()) {
            System.out.printf("%-" + lWidth + "s :  %-" + rWidth + "d\n", entry.getKey(), entry.getValue());
        }

        System.out.println("\n    ========================================     ");
    }

    private static void findMatchesPlayedPerSeason(List<Match> matches) {
        Map<String, Integer> matchesPlayedPerSeason = new HashMap<>();

        for (Match match : matches) {
            String season = match.getSeason();

            updateMapData(matchesPlayedPerSeason, season, 1);
        }

        printMapData(matchesPlayedPerSeason, "YEAR", "NO OF MATCHES", 10, 14);
    }

    private static void findNumberOfWinsPerTeam(List<Match> matches) {
        Map<String, Integer> numberOfWinsPerTeam = new HashMap<>();

        for (Match match : matches) {
            String result = match.getResult();

            if (result.equals("normal")) {
                String winner = match.getWinner();

                updateMapData(numberOfWinsPerTeam, winner, 1);
            }
        }

        printMapData(numberOfWinsPerTeam, "TEAM", "NO OF WINS", 30, 14);
    }

    private static void findPlayersWithAtLeast1PlayerOfMatch(List<Match> matches) {
        Map<String, Integer> playersWithAtLeast1PlayerOfMatch = new HashMap<>();

        for (Match match : matches) {
            String playerOfMatch = match.getPlayerOfMatch();

            if (!playerOfMatch.equals("")) {
                updateMapData(playersWithAtLeast1PlayerOfMatch, playerOfMatch, 1);
            }
        }

        System.out.println("    ========================================     \n");
        System.out.printf("%-30s : %-20s\n", "PLAYER NAME", "PLAYER OF MATCH AWARDS");
        playersWithAtLeast1PlayerOfMatch.entrySet().stream()
                .sorted((e1, e2) -> e2.getValue().compareTo(e1.getValue()))
                .forEach(e -> System.out.printf("%-30s : %-2d\n", e.getKey(), e.getValue()));
        System.out.println("\n    ========================================     ");
    }

    private static void findExtraRunsConcededByEachTeamIn2016(List<Delivery> deliveries) {
        Map<String, Integer> extraRunsConcededByEachTeamIn2016 = new HashMap<>();

        for (Delivery delivery : deliveries) {
            int matchId = delivery.getMatchId();
            String season = idSeasonMap.get(matchId);

            if (season.equals("2016")) {
                String bowlingTeam = delivery.getBowlingTeam();
                int extraRuns = delivery.getExtraRuns();

                updateMapData(extraRunsConcededByEachTeamIn2016, bowlingTeam, extraRuns);
            }
        }

        printMapData(extraRunsConcededByEachTeamIn2016, "TEAM", "EXTRA RUNS CONCEDED", 30, 13);
    }

    private static void findTopEconomicalBowlersIn2015(List<Delivery> deliveries) {

        Map<String, int[]> runsAndBallsByBowlersIn2015 = new HashMap<>();
        Map<String, Double> economiesOfBowlersIn2015 = new HashMap<>();

        for (Delivery delivery : deliveries) {
            int matchId = delivery.getMatchId();
            String season = idSeasonMap.get(matchId);

            if (season.equals("2015")) {
                int byeRuns = delivery.getByeRuns();
                int legByeRuns = delivery.getLegByeRuns();
                int totalRuns = delivery.getTotalRuns();
                int wideRuns = delivery.getWideRuns();
                int noBallRuns = delivery.getNoBallRuns();
                String bowler = delivery.getBowler();

                int countableRuns = totalRuns - byeRuns - legByeRuns;
                int countableBalls = wideRuns == 0 && noBallRuns == 0 ? 1 : 0;
                int[] bowlerStats = runsAndBallsByBowlersIn2015.getOrDefault(bowler, null);

                if (bowlerStats != null) {
                    bowlerStats[0] += countableRuns;
                    bowlerStats[1] += countableBalls;
                } else {
                    runsAndBallsByBowlersIn2015.put(bowler, new int[]{countableRuns, countableBalls});
                }
            }
        }

        for (Map.Entry<String, int[]> entry : runsAndBallsByBowlersIn2015.entrySet()) {
            String bowler = entry.getKey();
            int runs = entry.getValue()[0];
            double overs = entry.getValue()[1] / 6.0;
            economiesOfBowlersIn2015.put(bowler, runs / overs);
        }

        System.out.println("    ========================================     \n");
        System.out.printf("%-30s : %-14s\n", "BOWLER NAME", "ECONOMY");
        economiesOfBowlersIn2015.entrySet().stream()
                .sorted((e1, e2) -> e1.getValue().compareTo(e2.getValue()))
                .forEach(e -> System.out.printf("%-30s :  %-14.3f\n", e.getKey(), e.getValue()));
        System.out.println("\n    ========================================     ");
    }
}
