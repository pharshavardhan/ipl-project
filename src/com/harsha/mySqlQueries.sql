/*find no of matches played per year*/
SELECT 
  season, 
  COUNT(*) AS `No of matches`
FROM
  matches
GROUP BY season;
/*find matches won by each team*/
SELECT
  winner AS `Team name`,
  COUNT(*) AS `Matches won`
FROM
  matches
WHERE
  result = "normal"
GROUP BY
  winner;
/*find top 10 players with player of match descending order*/
SELECT
  player_of_match AS `Player name`,
  COUNT(*) AS `No of awards`
FROM
  matches
WHERE
  NOT player_of_match = ""
GROUP BY
  player_of_match
ORDER BY
  COUNT(*) DESC
LIMIT 10;
/*find extra runs conceded by each team in 2016*/
SELECT
  d.bowling_team AS `Team name`,
  SUM(d.extra_runs) as `Extra runs conceded`
FROM
  deliveries d
INNER JOIN 
  matches m USING(match_id)
WHERE
  m.season = "2016"
GROUP by
  d.bowling_team;
/*find economies of top 10 bowlers 2015*/
SELECT
  bowler,
  round(countable_runs / (total_balls / 6.0), 3) as economy
FROM (
  SELECT
    d.bowler,
    SUM(d.total_runs - d.bye_runs - d.leg_bye_runs) as countable_runs,
    SUM(CASE 
      WHEN d.no_ball_runs = 0 AND d.wide_runs = 0 THEN 1
      ELSE 0
    END) AS total_balls
  FROM 
    deliveries d
  INNER JOIN
    matches m USING(match_id)
  WHERE
    m.season = "2015"
  GROUP BY
    d.bowler
) bowler_runs_balls
ORDER BY 
  economy
LIMIT 10;
