package com.harsha;

/**
 * A class to store details of a cricket match
 *
 * @author Harsha
 */

class Match {
    private boolean isDlApplied;
    private int id;
    private int winByRuns;
    private int winByWickets;
    private String winner;
    private String season;
    private String city;
    private String date;
    private String team1;
    private String team2;
    private String tossWinner;
    private String tossDecision;
    private String result;
    private String playerOfMatch;
    private String venue;
    private String umpire1;
    private String umpire2;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setId(String id) {
        this.id = Integer.parseInt(id);
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTeam1() {
        return team1;
    }

    public void setTeam1(String team1) {
        if (team1.equals("Rising Pune Supergiants")) {
            team1 = "Rising Pune Supergiant";
        }
        this.team1 = team1;
    }

    public String getTeam2() {
        return team2;
    }

    public void setTeam2(String team2) {
        if (team2.equals("Rising Pune Supergiants")) {
            team2 = "Rising Pune Supergiant";
        }
        this.team2 = team2;
    }

    public String getTossWinner() {
        return tossWinner;
    }

    public void setTossWinner(String tossWinner) {
        if (tossWinner.equals("Rising Pune Supergiants")) {
            tossWinner = "Rising Pune Supergiant";
        }
        this.tossWinner = tossWinner;
    }

    public String getTossDecision() {
        return tossDecision;
    }

    public void setTossDecision(String tossDecision) {
        this.tossDecision = tossDecision;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public boolean isDlApplied() {
        return isDlApplied;
    }

    public void setDlApplied(boolean dlApplied) {
        isDlApplied = dlApplied;
    }

    public void setDlApplied(String dlApplied) {
        isDlApplied = dlApplied.equals("1");
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        if (winner.equals("Rising Pune Supergiants")) {
            winner = "Rising Pune Supergiant";
        }
        this.winner = winner;
    }

    public int getWinByRuns() {
        return winByRuns;
    }

    public void setWinByRuns(int winByRuns) {
        this.winByRuns = winByRuns;
    }

    public void setWinByRuns(String winByRuns) {
        this.winByRuns = Integer.parseInt(winByRuns);
    }

    public int getWinByWickets() {
        return winByWickets;
    }

    public void setWinByWickets(int winByWickets) {
        this.winByWickets = winByWickets;
    }

    public void setWinByWickets(String winByWickets) {
        this.winByWickets = Integer.parseInt(winByWickets);
    }

    public String getPlayerOfMatch() {
        return playerOfMatch;
    }

    public void setPlayerOfMatch(String playerOfMatch) {
        this.playerOfMatch = playerOfMatch;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getUmpire1() {
        return umpire1;
    }

    public void setUmpire1(String umpire1) {
        this.umpire1 = umpire1;
    }

    public String getUmpire2() {
        return umpire2;
    }

    public void setUmpire2(String umpire2) {
        this.umpire2 = umpire2;
    }
}
