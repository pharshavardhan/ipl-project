# ipl-project : Exercise in mountblue core java curicculum

**[IPL-DATASET(2008-2017)](https://www.kaggle.com/manasgarg/ipl)**

- `src/com/harsha/Match.java` stores data of a single Match and `src/com/harsha/Delivery.java` stores data of single delivery.
- `src/com/harsha/Main.java` processes datasets `deliveries.csv` and `matches.csv` by using above classes, prints below results to standard output: 
  
  1. Number of matches played per year in IPL.
  2. Number of matches won by each team over all years given in dataset.
  3. Number of extra runs conceded by each team in 2016.
  4. Economies of every bowler sorted in ascending order in 2015.
  5. Number of player of match awards won by each player(atleat 1) in descending order over all years given.
